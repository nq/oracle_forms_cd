#!/usr/bin/env bash

PAYX_USER=${PAYX_USER:-''}
PASSWORD=${PASSWORD:-''}

[ ! -z "$PAYX_USER" ] || (echo "ERROR: Please set your paychex username " && exit 1)
[ ! -z "$PASSWORD" ] || (echo "You need to set your password!" && exit 1)

#############################################################
echo '#######################################################'
echo '#                  Update the VM                      #'
echo '#######################################################'

sudo rm /etc/yum.repos.d/*

echo '# CentOS-Vault.repo
#---- Packages previously released as 5.11, and its updates
[C5.11-base]
name=CentOS-5.11 - Base
baseurl=http://vault.centos.org/5.11/os/$basearch/
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-5

[C5.11-updates]
name=CentOS-5.11 - Updates
baseurl=http://vault.centos.org/5.11/updates/$basearch/
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-5

[C5.11-extras]
name=CentOS-5.11 - Extras
baseurl=http://vault.centos.org/5.11/extras/$basearch/
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-5

[C5.11-centosplus]
name=CentOS-5.11 - Plus
baseurl=http://vault.centos.org/5.11/centosplus/$basearch/
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-5
' | sudo tee /etc/yum.repos.d/CentOS-Vault.repo

wget --progress=bar:force http://mirror.city-fan.org/ftp/contrib/yum-repo/CITY-FAN.ORG-GPG-KEY
sudo rpm --import -v CITY-FAN.ORG-GPG-KEY

wget --progress=bar:force http://mirror.city-fan.org/ftp/contrib/yum-repo/rhel5/i386/city-fan.org-release-1-13.rhel5.noarch.rpm
sudo rpm -Uvh city-fan.org-*.noarch.rpm

sudo yum update -y

#############################################################
echo '#######################################################'
echo '#               Install dependencies                  #'
echo '#######################################################'

sudo yum install -y libXp libXt gcc gcc-c++ pdksh sysstat compat-libstdc++-296 compat-db make setarch glibc-devel
sudo yum update -y curl

# Uncomment if for some reason you need to do GUI stuff with the Oracle forms installer.
# sudo yum install -y libXtst xauth
# sudo unlink /usr/lib/libXtst.so.6

wget --progress=bar:force https://oss.oracle.com/projects/compat-oracle/dist/files/Enterprise_Linux/xorg-x11-libs-compat-6.8.2-1.EL.33.0.1.i386.rpm
sudo rpm -iv xorg-x11-libs-compat-*.rpm

# Not sure if this is needed, keeping as a comment just in case. 
# curl -O ftp://ftp.icm.edu.pl/vol/rzm6/linux-scientificlinux/obsolete/40/i386/errata/SL/RPMS/openmotif21-2.1.30-11.RHEL4.4.i386.rpm
# sudo rpm -iv openmotif21-*.rpm

#############################################################
echo '#######################################################'
echo '#            Download installation media              #'
echo '#######################################################'

curl -OO -u "paychex\\$PAYX_USER:$PASSWORD" smb://corpnt01/global_sh/wlaw/mirror/as_linux_x86_ids_101202_disk{1,2}.cpio

#############################################################
echo '#######################################################'
echo '#             Extract installation media              #'
echo '#######################################################'

cpio -icd < as_linux_x86_ids_101202_disk1.cpio
cpio -icd < as_linux_x86_ids_101202_disk2.cpio

#############################################################
echo '#######################################################'
echo '#              Patch installation media               #'
echo '#######################################################'

# Patch the installer so that it doesn't run as a background process
printf '\x90\x90' | dd of=/home/vagrant/Disk1/install/runInstaller bs=1 seek=22868 count=2 conv=notrunc 

#############################################################
echo '#######################################################'
echo '#          Prepare system for installation            #'
echo '#######################################################'

export PATH=/usr/sbin:/sbin/:$PATH

sudo mkdir -p /opt/oracle/oraInventory
sudo chown -R vagrant:vagrant /opt/oracle
sudo chmod 775 /opt/oracle/oraInventory

echo 'inventory_loc=/opt/oracle/oraInventory
inst_group=vagrant
' | sudo tee /etc/oraInst.loc
 
# Need to make Oracle think we're on RHEL4. 
sudo cp /etc/redhat-release /etc/redhat-release.5
echo redhat-4 | sudo tee /etc/redhat-release

# Link the X11 compatibility lib manually since the RPM doesn't do it for us. 
sudo ln -s /usr/X11R6/lib/libXtst.so.6.1 /usr/lib/libXtst.so.6

echo 'kernel.msgmni = 2878
fs.file-max = 206173
kernel.sem = 256 32000 100 142
' | sudo tee -a /etc/sysctl.conf

sudo sysctl -p

#############################################################
echo '#######################################################'
echo '# Generate automated installation configuration file  #'
echo '#######################################################'

echo 'RESPONSEFILE_VERSION=2.2.1.0.0
ACCEPT_LICENSE_AGREEMENT=true
b_olite_install=true
COMPONENT_LANGUAGES={"en"}
FROM_LOCATION="/home/vagrant/Disk1/stage/products.xml"
FROM_LOCATION_CD_LABEL="LABEL1"
INSTALL_TYPE="Complete"
LOCATION_FOR_DISK2="/home/vagrant/Disk2"
mailServerName=""
NEXT_SESSION=false
NEXT_SESSION_ON_FAIL=true
ORACLE_HOME="/opt/oracle/dev"
ORACLE_HOME_NAME="dev"
RESTART_SYSTEM=false
TOPLEVEL_COMPONENT={"oracle.ids.toplevel.development","10.1.2.0.2"}
UNIX_GROUP_NAME=vagrant
' | sudo tee /home/vagrant/Disk1/complete_install.rsp

#############################################################
echo '#######################################################'
echo '#   Execute the oracle dev installation script here   #'
echo '#######################################################'

linux32 /home/vagrant/Disk1/runInstaller -silent -ignoreSysPrereqs -responseFile /home/vagrant/Disk1/complete_install.rsp

#############################################################
echo '#######################################################'
echo '#            Post-install configuration               #'
echo '#######################################################'

sed -i '/$ECHO $N "Enter the full pathname of the local bin directory: $C"/ { c \
#$ECHO $N "Enter the full pathname of the local bin directory: $C"
}' /opt/oracle/dev/root.sh

sed -i '/DEFLT=${LBIN}; . $ORACLE_HOME\/install\/utl\/read.sh; LBIN=$RDVAR/ { c \
#DEFLT=${LBIN}; . $ORACLE_HOME\/install\/utl\/read.sh; LBIN=$RDVAR
}' /opt/oracle/dev/root.sh

sudo /opt/oracle/dev/root.sh
export ORACLE_HOME=/opt/oracle/dev
export LD_LIBRARY_PATH=$ORACLE_HOME/lib
/opt/oracle/dev/cfgtoollogs/configToolCommands

