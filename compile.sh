export ORACLE_HOME=/opt/oracle/10.1.2
export PATH=$ORACLE_HOME/bin:$PATH
export term=vt220
export ORACLE_TERM=vt220
export LD_LIBRARY_PATH=$ORACLE_HOME/jdk/jre/lib/i386/server:$ORACLE_HOME/jdk/jre/lib/i386/client:$ORACLE_HOME/jre/1.4.2/lib/i386/server:$ORACLE_HOME/jre/1.4.2/lib/i386/client
db=ddsc11g
type=FORM
app=dds
frmcmp_batch.sh Module=$1 Userid=dds/dds@"$db" Module_Type=$type
