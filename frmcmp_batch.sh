#!/bin/sh
# frmcmp_batch.sh - executable Bourne shell script to run the Forms Compiler
#                   (Generator) in batch mode after setting up the required
#                   environment
#
# NOTES
# 1/ The Forms installation process should replace
#    <percent>FORMS_ORACLE_HOME<percent> with the correct ORACLE_HOME
#    setting.  Please make these changes manually if not.
# 2/ Some of the variables below may need to be changed to suite your needs.
#    Please refer to the Forms documentation for details.
#

# Set ORACLE_HOME if not set in the calling environment:
ORACLE_HOME=${ORACLE_HOME:-/opt/oracle/10.1.2}
export ORACLE_HOME

  #
  # Search path for Forms applications (.fmb &.fmx files, PL/SQL libraries)
  # If you need to include more than one directory, they should be colon
  # separated (e.g. /private/dir1:/private/dir2)
  # Note: the current directory is always searched by default
  #
  # FORMS_PATH=< your application directory 1>:<etc>
  # export FORMS_PATH

  #
  # You may need to set one or more of TNS_ADMIN, TWO_TASK or ORACLE_SID
  # to connect to database
  #
  TNS_ADMIN=$ORACLE_HOME/network/admin
  export TNS_ADMIN
  #TWO_TASK=<your database connect string>; export TWO_TASK
  #ORACLE_SID=< ORACLE SID >; export ORACLE_SID

  #
  # System settings
  # ---------------
  # You should not normally need to modify these settings.
  #
  if [ `uname -s` = 'SunOS' ]; then
    LD_LIBRARY_PATH=$ORACLE_HOME/lib32:$ORACLE_HOME/jdk/jre/lib/sparc:$ORACLE_HOME/jdk/jre/lib/sparc/native_threads:$LD_LIBRARY_PATH

    export LD_LIBRARY_PATH
  elif [ `uname -s` = 'HP-UX' ]; then
    SHLIB_PATH=$ORACLE_HOME/lib32:$ORACLE_HOME/jdk/jre/lib/PA_RISC:$ORACLE_HOME/jdk/jre/lib/PA_RISC/server:$SHLIB_PATH
      export SHLIB_PATH
  elif [ `uname -s` = 'Linux' ]; then
    LD_LIBRARY_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/jdk/jre/lib/i386/server:$ORACLE_HOME/jdk/jre/lib/i386/native_threads:$ORACLE_HOME/jdk/jre/lib/i386:$LD_LIBRARY_PATH
    export LD_LIBRARY_PATH
  elif [ `uname -s` = 'SunOs' ]; then
    LD_LIBRARY_PATH=$ORACLE_HOME/lib32:$ORACLE_HOME/jdk/jre/lib/sparc:$ORACLE_HOME/jdk/jre/lib/sparc/native_threads:$LD_LIBRARY_PATH

    export LD_LIBRARY_PATH
  else
    LD_LIBRARY_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/jdk/jre/lib/sparc:$ORACLE_HOME/jdk/jre/lib/sparc/native_threads:$LD_LIBRARY_PATH
      export LD_LIBRARY_PATH
  fi
$ORACLE_HOME/bin/frmcmp_batch $*