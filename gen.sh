#!/bin/sh
#-------------------------------------------------------------------------
# Script Name: fgen
#
# Purpose: This script will generate a Oracle Forms 10g FMX file.
#
# Usage: fgen filename
#             Note: Do not include file extension.
#-------------------------------------------------------------------------

export ORACLE_HOME=/opt/oracle/10.1.2
export PATH=$ORACLE_HOME/bin:$PATH
export DISPLAY=192.168.56.1:0.0
#export DISPLAY=192.168.1.4:0.0
export term=vt220
export ORACLE_TERM=vt220
echo " "
print " "
print -n "Which database? [tpsc11g,ddsc11g,ntwc11g]:"
read TWO_TASK
print " "
export LD_LIBRARY_PATH=$ORACLE_HOME/jdk/jre/lib/i386/server:$ORACLE_HOME/jdk/jre/lib/i386/client:$ORACLE_HOME/jre/1.4.2/lib/i386/server:$ORACLE_HOME/jre/1.4.2/lib/i386/client
print -n "Select a Module Type [FORM,MENU]: "
read type
typeset -u type
echo $type
print " "
print -n "Enter a system [car,yem,dds,tps,common,cps,paws,appsec,wire,nwh,svp]: "
read system
echo $system
print " "
if [[ $type = "FORM" ]] then
  echo "Generating FMX file..."
fi
if [[ $type = "MENU" ]] then
  echo "Generating MMX file..."
fi

if [[ $system = "tps" ]] then
  frmcmp_batch.sh Module=$1 Userid=impound/impound@"$TWO_TASK" Module_Type=$type
fi
if [[ $system = "yem" ]] then
  frmcmp_batch.sh Module=$1 Userid=yem/yem@"$TWO_TASK" Module_Type=$type
fi
if [[ $system = "car" ]] then
  frmcmp_batch.sh Module=$1 Userid=car/car@"$TWO_TASK" Module_Type=$type
fi
if [[ $system = "dds" ]] then
  frmcmp_batch.sh Module=$1 Userid=dds/dds@"$TWO_TASK" Module_Type=$type
fi
if [[ $system = "ech" ]] then
  frmcmp_batch.sh Module=$1 Userid=echo/echo@"$TWO_TASK" Module_Type=$type
fi
if [[ $system = "common" ]] then
  frmcmp_batch.sh Module=$1 Userid=common/common@"$TWO_TASK" Module_Type=$type
fi
if [[ $system = "cps" ]] then
  frmcmp_batch.sh Module=$1 Userid=cps/cps@"$TWO_TASK" Module_Type=$type
fi
if [[ $system = "paws" ]] then
  frmcmp_batch.sh Module=$1 Userid=paws/paws@"$TWO_TASK" Module_Type=$type
fi
if [[ $system = "appsec" ]] then
  frmcmp_batch.sh Module=$1 Userid=appsec/appsec@"$TWO_TASK" Module_Type=$type
fi
if [[ $system = "wire" ]] then
 frmcmp_batch.sh Module=$1 Userid=wire/wire@"$TWO_TASK" Module_type=$type
fi
if [[ $system = "nwh" ]] then
 frmcmp_batch.sh Module=$1 Userid=impound/impound@"$TWO_TASK" Module_type=$type
fi
if [[ $system = "svp" ]] then
 frmcmp_batch.sh Module=$1 Userid=svp/svp@"$TWO_TASK" Module_type=$type
fi
echo " "
if [[ $type = "FORM" ]]
then
  if [[ -a $1.fmx ]]
  then
    echo "$1.fmx has been successfully generated."
    mv $1.fmx /opt/appl/forms/beta/$system
    echo "$1.fmx has been moved to /opt/appl/forms/beta/$system"
    rm $1.err
  else
    echo "Error generating $1.fmx"
    if [[ -a $1.err ]] then
      echo "Error file $1.err has been created"
    fi
  fi
fi
if [[ $type = "MENU" ]]
then
  if [[ -a $1.mmx ]]
  then
    mv $1.mmx /opt/appl/forms/beta/$system
    echo "$1.mmx has been successfully generated."
    echo "$1.mmx has been moved to /opt/appl/forms/beta/$system"
  else
    echo "Error generating $1.mmx"
    if [[ -a $1.err ]] then
      echo "Error file $1.err has been created"
    fi
  fi
fi
echo " "