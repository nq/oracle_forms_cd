#!/usr/bin/env bash
set -e 
# set -x

arguments() {
	echo >&2 'Usage: fakemake.sh {(compile|deploy) [file]|initssh}'
	exit 1
}

[[ -n $1 && -n $2 ]] || arguments

script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
filename_fmb="$(basename $2)"
filename_fmx="${filename_fmb%.*}.fmx"
remote_hostname="orapdvw1"

# Target definitions go here. Put dependencies in your task definitions.
compile() {

	generateCompileScript() {
		 cmd="export ORACLE_HOME=/opt/oracle/10.1.2; "
		cmd+="export PATH=\$ORACLE_HOME/bin:\$PATH; "
		cmd+="export term=vt220; "
		cmd+="export ORACLE_TERM=vt220; "
		cmd+="export LD_LIBRARY_PATH=\$ORACLE_HOME/jdk/jre/lib/i386/server:\$ORACLE_HOME/jdk/jre/lib/i386/client:\$ORACLE_HOME/jre/1.4.2/lib/i386/server:\$ORACLE_HOME/jre/1.4.2/lib/i386/client; "
		cmd+="db=ddsc11g; "		
		cmd+="type=FORM; "
		cmd+="app=dds; "
		cmd+="frmcmp_batch.sh Module=$1 Userid=dds/dds@\"\$db\" Module_Type=\$type"
		
		echo "$cmd"
	}

	echo "--- Copying sources to $remote_hostname..."
	scp $script_dir/$filename_fmb $USERNAME@$remote_hostname:$filename_fmb
	scp $script_dir/../../tps/fmb/MENU.OLB $USERNAME@$remote_hostname:MENU.OLB

	echo "--- Compiling..."
	ssh $USERNAME@$remote_hostname "$(generateCompileScript $filename_fmb)"

	echo "--- Copying compiled FMX to repo..."
	scp $USERNAME@$remote_hostname:$filename_fmx $script_dir/../fmx/

	echo "--- Removing files from $remote_hostname..."
	ssh $USERNAME@$remote_hostname "rm $filename_fmb $filename_fmx MENU.OLB"
}

deploy() {
	compile

	echo "--- Copying FMX to serve folder on $remote_hostname..."
	scp $script_dir/../fmx/$filename_fmx $USERNAME@$remote_hostname:$filename_fmx
	ssh $USERNAME@$remote_hostname "mv $filename_fmx /opt/appl/forms/dds/"
}
# End, all task definitions

# Update this switch with your targets:
case "$1" in
	"compile") compile ;;
	"deploy" ) deploy ;;
	*) arguments ;;
esac

